---
title: About this project
subtitle: Why you'd need to use asciidoc syntax in LabVIEW
comments: true
---

This project was born in mid 2019 in order to be use in another open source project that generate code documentation for LabVIEW project.


### About AsciiDoc

From  [AsciiDoc](http://www.methods.co.nz/asciidoc/index.html#_introduction)

> AsciiDoc is a text document format for writing notes, documentation, articles, books, ebooks, slideshows, web pages, man pages and blogs. AsciiDoc files can be translated to many formats including HTML, PDF, EPUB, man page.

### About asciidoctor

From [Asciidoctor](https://asciidoctor.org/)

> Asciidoctor is a fast, open source text processor and publishing toolchain for converting AsciiDoc content to HTML5, DocBook, PDF, and other formats. 

